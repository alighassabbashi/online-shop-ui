export const state = () => ({
  segments: [],
  brands: [],
})

export const mutations = {
  GET_SEGMENTS(state, segments) {
    state.segments = segments
  },
  GET_BRANDS(state, brands) {
    state.brands = brands
  },
  EDIT_SEGMENT(state, editedSegment) {
    const segmentIndex = state.segments.findIndex(
      (segment) => segment._id === editedSegment._id
    )
    state.segments[segmentIndex].name = editedSegment.name
    console.log(state.segments[segmentIndex].name)
  },
  CATEGORY_DELITION(state, ids) {
    const segmentIndex = state.segments.findIndex(
      (segment) => segment._id === ids.segmentId
    )
    const categoryIndex = state.segments[segmentIndex].categories.findIndex(
      (category) => category._id === ids.categoryId
    )
    state.segments[segmentIndex].categories.splice(categoryIndex, 1)
  },
  SEGMENT_DETITION(state, segmentId) {
    const segmentIndex = state.segments.findIndex(
      (segment) => segment._id === segmentId
    )
    state.segments.splice(segmentIndex, 1)
  },
}

export const actions = {
  async getSegments({ commit }) {
    const segments = await this.$axios.$get('/shop/segment')
    commit('GET_SEGMENTS', segments)
  },
  async getBrands({ commit }) {
    const brands = await this.$axios.$get('/admin/brand/all')
    commit('GET_BRANDS', brands)
  },
  async editTitle({ commit }, segment) {
    try {
      const editedSegment = await this.$axios.$patch(
        `/admin/segment/${segment.segmentId}`,
        { name: segment.editedTitle }
      )
      commit('EDIT_SEGMENT', editedSegment)
      return editedSegment.name
    } catch (err) {
      console.log('error updating segment title', err)
    }
  },
  async categoryDelition({ commit }, ids) {
    try {
      const message = await this.$axios.$delete(
        `/admin/segment/deleteCategory/${ids.segmentId}`,
        { categoryId: ids.categoryId }
      )
      commit('CATEGORY_DELITION', ids)
      return message
    } catch (err) {
      console.log('error dateting category', err)
    }
  },
  async segmentDelition({ commit }, segmentId) {
    try {
      const message = await this.$axios.$delete(`/admin/segment/${segmentId}`)
      commit('SEGMENT_DETITION', segmentId)
      return message
    } catch (err) {
      console.log('error deleting segment', err)
    }
  },
}

export const getters = {
  getSegments(state) {
    return state.segments
  },
  getBrands(state) {
    return state.brands
  },
}
